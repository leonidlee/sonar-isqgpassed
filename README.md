# Check your project passes SonarQube Quality Gate
You need to have sonar-scanner temporary directory with report file in it.
Actually this file is an auto generated file that is available once the scan process is completed.

Report file location on:
 - linux (command line): .scannerwork/report-task.txt.
 - windows (msbuild plugin): .sonarqube/out/.sonar/report-task.txt

Default timeout for sonar calls is 15 seconds.

Script tries to fetch data 10 times a row. On success it skips further tries. On fail it exits with exit code 1.

### Prerequisites:
You need python 2.7 to run this script.
 

### Usage:
`is_quality_gate_passed.py ${report_file} [-t, --timeout] ${timeout}`


#### Examples:
`is_quality_gate_passed.py .scannerwork/report-task.txt`

`is_quality_gate_passed.py .scannerwork/report-task.txt -t 25`

`is_quality_gate_passed.py .scannerwork/report-task.txt --timeout 5`