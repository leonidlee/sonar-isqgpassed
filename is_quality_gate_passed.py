#!/bin/env python
# -*- coding: utf-8 -*-


import os.path
import re
import urllib2
import json
import time
import argparse
import ssl


parser = argparse.ArgumentParser(description='''Check your project meets SonarQube Quality Gate metrics.''')
parser.add_argument('file', default='.scannerwork/report-task.txt',
                    help='Path to sonar-scanner report. Default value: ".scannerwork/report-task.txt"')
parser.add_argument('-t', '--timeout', default='15', type=float, help='Timeout in seconds')
args = parser.parse_args()
REPORT_FILE = args.file
timeout = args.timeout
max_tries = 10


def get_url(url):
    for i in range(1, max_tries):
        try:
            print("Trying to get URL:")
            print(url)
            ssl._create_default_https_context = ssl._create_unverified_context
            html = urllib2.urlopen(url).read()
            return html
        except urllib2.HTTPError as e:
            print("Error code:")
            print(e.code)
            print("Waiting for next try...")
            time.sleep(timeout)
            continue
        except urllib2.URLError as e:
            print(e.reason)
            print("Wrong URL")
            exit(1)
    print("ERROR: cannot get url:")
    print(url)
    exit(1)


def get_records(file_path):
    records = []
    if not os.path.isfile(file_path):
        err_msg = "Error: %s not found" % file_path
        print(err_msg)
    try:
        with open(file_path, "r") as report_file:
            lines = report_file.read()
        if len(lines) == 0:
            print("Error: File is empty or corrupted")
            exit(1)
        new_lines = lines.split("\n")
        for line in new_lines:
            res = line.split("=", 1)
            if res[0] == "serverUrl":
                records.append(res[1])
            if res[0] == "ceTaskUrl":
                records.append(res[1])
    except IOError:
        print "Error: File does not appear to exist."
    return records


def get_quality_gate_result(server_url, url):
    print("Trying to get QG report.")
    for i in range(1, max_tries):
        print("Attempt:")
        print(i)
        analysis_body = get_url(url)
        analysis_json = json.loads(analysis_body)
        if "task" not in analysis_json or "analysisId" not in analysis_json["task"]:
            if i == 10:
                print("Error: No data fetched.")
                exit(1)
            time.sleep(timeout)
            continue
        else:
            break
    qg_url = [server_url, '/api/qualitygates/project_status?analysisId=', analysis_json["task"]["analysisId"]]
    quality_gate = get_url("".join(qg_url))
    return re.findall(r'"status":"ERROR"', quality_gate)


def main():
    urls = get_records(REPORT_FILE)
    matches = get_quality_gate_result(urls[0], urls[1])
    if matches is None or len(matches) == 0:
        print("Quality gate passed.")
        exit(0)
    print("Quality gate not passed.")
    print("Check your project:")
    print(urls[1])
    exit(1)


if __name__ == '__main__':
    main()
